package no.uib.gre002.info233.v2015.oblig2.gui.controllers.utilities;

import java.util.Collection;
import java.util.HashMap;

/**
 * Support class with static methods for populating the room and building lists
 * 
 * @author Anders Eide
 *
 */
public class ListPopulator {
	private static HashMap<String,HashMap<String, String>> buildingMap;
	
	public static Collection<String> getBuildings(){
		return buildingMap.keySet();
	}
	
	public static Collection<String> getRooms(String building){
		return buildingMap.get(building).keySet();
	}
	
	public static String getRoomURL(String building, String room){
		return buildingMap.get(building).get(room);
	}
	
	/**
	 * Method used to initialize the list of rooms and buildings
	 * 
	 * Warning: Here be dragons
	 */
	public static void initialize(){
		
		buildingMap = new HashMap<String, HashMap<String, String>>();
		
		buildingMap.put("Bj�rn Trumpys hus (Fysikkbygget)", new HashMap<String, String>());
		buildingMap.get("Bj�rn Trumpys hus (Fysikkbygget)").put("Laboratorium 260", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=A55%3A&room=A55%3AG260");
		buildingMap.get("Bj�rn Trumpys hus (Fysikkbygget)").put("Grupperom 292", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=A55%3A&room=A55%3AG292");
		buildingMap.get("Bj�rn Trumpys hus (Fysikkbygget)").put("Grupperom 316", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=A55%3A&room=A55%3AG316");
		buildingMap.get("Bj�rn Trumpys hus (Fysikkbygget)").put("Grupperom 366", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=A55%3A&room=A55%3AG366");
		buildingMap.get("Bj�rn Trumpys hus (Fysikkbygget)").put("Grupperom 368", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=A55%3A&room=A55%3AG368");
		buildingMap.get("Bj�rn Trumpys hus (Fysikkbygget)").put("Grupperom 546", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=A55%3A&room=A55%3AG546");

		buildingMap.put("All�gaten 66", new HashMap<String, String>());
		buildingMap.get("All�gaten 66").put("Auditorium A", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=A66%3A&room=A66%3AAA");
		buildingMap.get("All�gaten 66").put("Auditorium B", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=A66%3A&room=A66%3AAB");
		
		buildingMap.put("All�gaten 70 (Geofysen)", new HashMap<String, String>());
		buildingMap.get("All�gaten 70 (Geofysen)").put("Foredragssal 200 (234)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=A70%3A&room=A70%3AA200");
		buildingMap.get("All�gaten 70 (Geofysen)").put("Lite m�terom (329)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=A70%3A&room=A70%3ALM3");
		buildingMap.get("All�gaten 70 (Geofysen)").put("Stort m�terom (325)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=A70%3A&room=A70%3ASM3");
		
		buildingMap.put("Armauer Hansens hus", new HashMap<String, String>());
		buildingMap.get("Armauer Hansens hus").put("Kollokvierom 106", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=AHH%3A&room=AHH%3A106");
		buildingMap.get("Armauer Hansens hus").put("Kollokvierom 108", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=AHH%3A&room=AHH%3A108");
		buildingMap.get("Armauer Hansens hus").put("Kollokvierom 110", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=AHH%3A&room=AHH%3A110");
		buildingMap.get("Armauer Hansens hus").put("Kollokvierom 112", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=AHH%3A&room=AHH%3A112");
		buildingMap.get("Armauer Hansens hus").put("Kollokvierom 120", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=AHH%3A&room=AHH%3A120");
		buildingMap.get("Armauer Hansens hus").put("Kollokvierom 1121", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=AHH%3A&room=AHH%3A1121");
		buildingMap.get("Armauer Hansens hus").put("Kollokvierom 1122", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=AHH%3A&room=AHH%3A1122");
		
		buildingMap.put("BB-bygget", new HashMap<String, String>());
		buildingMap.get("BB-bygget").put("PC-stue 9A108dP", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3A9A108");
		buildingMap.get("BB-bygget").put("Grupperom 9A109bP", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3A9A109");
		buildingMap.get("BB-bygget").put("Grupperom 9A110bP", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3A9A110");
		buildingMap.get("BB-bygget").put("Auditorium 1 (3C120F)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AA1");
		buildingMap.get("BB-bygget").put("Auditorium 2 (3C122F)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AA2");
		buildingMap.get("BB-bygget").put("Auditorium 4 (3C132F)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AA4");
		buildingMap.get("BB-bygget").put("Disseksjonssal", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3ADIS");
		buildingMap.get("BB-bygget").put("Grupperom 1 (119F)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AG1");
		buildingMap.get("BB-bygget").put("Grupperom 2 (123F)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AG2");
		buildingMap.get("BB-bygget").put("Grupperom 3 (127F)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AG3");
		buildingMap.get("BB-bygget").put("Grupperom 4 (131F)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AG4");
		buildingMap.get("BB-bygget").put("Grupperom 5 (135F)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AG5");
		buildingMap.get("BB-bygget").put("Grupperom 6 (139F)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AG6");
		buildingMap.get("BB-bygget").put("Grupperom 7 (143F)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AG7");
		buildingMap.get("BB-bygget").put("Histologisal 1", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AHIST1");
		buildingMap.get("BB-bygget").put("Histologisal 2", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AHIST2");
		buildingMap.get("BB-bygget").put("Histologisal 3", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AHIST3");
		buildingMap.get("BB-bygget").put("Konferanserom (3B109F)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AKF109F");
		buildingMap.get("BB-bygget").put("Kurssal 1", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AKURS1");
		buildingMap.get("BB-bygget").put("Kurssal 2", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AKURS3");
		buildingMap.get("BB-bygget").put("Laboratorium 4. etasje", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3ALAB");
		buildingMap.get("BB-bygget").put("Laboratorium 1 (Fl�yen)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3ALAB1");
		buildingMap.get("BB-bygget").put("Laboratorium 2 (L�vstakken)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3ALAB2");
		buildingMap.get("BB-bygget").put("Laboratorium 3 (Ulriken)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3ALAB3");
		buildingMap.get("BB-bygget").put("Vrimleareal", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BB%3A&room=BB%3AVRIMLE");
		
		buildingMap.put("Bj�rn Christiansens hus (Christies gate 12)", new HashMap<String, String>());
		buildingMap.get("Bj�rn Christiansens hus (Christies gate 12)").put("PC stue 021", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BC%3A&room=BC%3A021");
		buildingMap.get("Bj�rn Christiansens hus (Christies gate 12)").put("PC stue 022", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BC%3A&room=BC%3A022");
		buildingMap.get("Bj�rn Christiansens hus (Christies gate 12)").put("PC stue 023", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BC%3A&room=BC%3A023");
		buildingMap.get("Bj�rn Christiansens hus (Christies gate 12)").put("Grupperom 110", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BC%3A&room=BC%3A110");
		buildingMap.get("Bj�rn Christiansens hus (Christies gate 12)").put("Seminarrom 111", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BC%3A&room=BC%3A111");
		buildingMap.get("Bj�rn Christiansens hus (Christies gate 12)").put("Auditorium 118", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BC%3A&room=BC%3A118");
		buildingMap.get("Bj�rn Christiansens hus (Christies gate 12)").put("Auditorium 130", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BC%3A&room=BC%3A130");
		buildingMap.get("Bj�rn Christiansens hus (Christies gate 12)").put("Seminarrom 131", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BC%3A&room=BC%3A131");
		buildingMap.get("Bj�rn Christiansens hus (Christies gate 12)").put("Seminarrom 132", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BC%3A&room=BC%3A131");
		buildingMap.get("Bj�rn Christiansens hus (Christies gate 12)").put("Seminarrom 135", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BC%3A&room=BC%3A135");
		buildingMap.get("Bj�rn Christiansens hus (Christies gate 12)").put("Seminarrom 136", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BC%3A&room=BC%3A136");
		buildingMap.get("Bj�rn Christiansens hus (Christies gate 12)").put("Seminarrom 215", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BC%3A&room=BC%3A215");
		buildingMap.get("Bj�rn Christiansens hus (Christies gate 12)").put("Seminarrom 520", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BC%3A&room=BC%3A520");
		buildingMap.get("Bj�rn Christiansens hus (Christies gate 12)").put("Seminarrom 555", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BC%3A&room=BC%3A555");
		
		buildingMap.put("Biologen", new HashMap<String, String>());
		buildingMap.get("Biologen").put("A-blokken, rom K1 (1C17)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BIO%3A&room=BIO%3AK1");
		buildingMap.get("Biologen").put("A-blokken, rom K2 (1B16)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BIO%3A&room=BIO%3AK2");
		buildingMap.get("Biologen").put("B-blokken, rom K3 (1H19)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BIO%3A&room=BIO%3AK3");
		buildingMap.get("Biologen").put("B-blokken, rom K4 (1G19)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BIO%3A&room=BIO%3AK4");
		buildingMap.get("Biologen").put("Liten sal A (1C05)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BIO%3A&room=BIO%3ALITENSALA");
		buildingMap.get("Biologen").put("Liten sal B (1G14)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BIO%3A&room=BIO%3ALITENSALB");
		buildingMap.get("Biologen").put("Stor sal A (1C09)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BIO%3A&room=BIO%3ASTORSALA");
		buildingMap.get("Biologen").put("Stor sal B, (1G07)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BIO%3A&room=BIO%3ASTORSALB");
		
		buildingMap.put("Barneklinikken", new HashMap<String, String>());
		buildingMap.get("Barneklinikken").put("Auditorium Pediatr. inst. (1073A)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BK%3A&room=BK%3APED");
		
		buildingMap.put("Brakkerigg Vinjesgate", new HashMap<String, String>());
		buildingMap.get("Brakkerigg Vinjesgate").put("Seminarrom A", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BV%3A&room=BV%3ASA");
		buildingMap.get("Brakkerigg Vinjesgate").put("Seminarrom B", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BV%3A&room=BV%3ASB");
		buildingMap.get("Brakkerigg Vinjesgate").put("Seminarrom C", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BV%3A&room=BV%3ASC");
		buildingMap.get("Brakkerigg Vinjesgate").put("Seminarrom D", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=BV%3A&room=BV%3ASD");
		
		buildingMap.put("Christies gate 13 (Vekterg�rden)", new HashMap<String, String>());
		buildingMap.get("Christies gate 13 (Vekterg�rden)").put("Seminarrom 204", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=C13%3A&room=C13%3A204");
		buildingMap.get("Christies gate 13 (Vekterg�rden)").put("Seminarrom 207", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=C13%3A&room=C13%3A207");
		buildingMap.get("Christies gate 13 (Vekterg�rden)").put("Seminarrom 251", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=C13%3A&room=C13%3A251");
		buildingMap.get("Christies gate 13 (Vekterg�rden)").put("Rom 276-1", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=C13%3A&room=C13%3A276-1");
		buildingMap.get("Christies gate 13 (Vekterg�rden)").put("Rom 276-2 / 276-3", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=C13%3A&room=C13%3A276-2-3");
		buildingMap.get("Christies gate 13 (Vekterg�rden)").put("Rom 276-4", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=C13%3A&room=C13%3A276-4");
		buildingMap.get("Christies gate 13 (Vekterg�rden)").put("Rom 277-1", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=C13%3A&room=C13%3A277-1");
		buildingMap.get("Christies gate 13 (Vekterg�rden)").put("Rom 278-1", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=C13%3A&room=C13%3A278-1");
		buildingMap.get("Christies gate 13 (Vekterg�rden)").put("Rom 278-2", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=C13%3A&room=C13%3A278-2");
		buildingMap.get("Christies gate 13 (Vekterg�rden)").put("Rom 278-2 / Rom 278-3", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=C13%3A&room=C13%3A278-2-3");
		buildingMap.get("Christies gate 13 (Vekterg�rden)").put("Rom 278-3", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=C13%3A&room=C13%3A278-3");
		buildingMap.get("Christies gate 13 (Vekterg�rden)").put("M�terom 331", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=C13%3A&room=C13%3A331");
		
		buildingMap.put("Christies gate 17", new HashMap<String, String>());
		buildingMap.get("Christies gate 17").put("Kursrom 17.215", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=C17%3A&room=C17%3AK215");
		buildingMap.get("Christies gate 17").put("Seminarrom 17.112", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=C17%3A&room=C17%3AS112");
		
		buildingMap.put("Carl L. Godskes hus", new HashMap<String, String>());
		buildingMap.get("Carl L. Godskes hus").put("Auditorium 307 (\"pi\")", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=CLG%3A&room=CLG%3AA307");
		buildingMap.get("Carl L. Godskes hus").put("Kollokvierom 402 (\"alfa\")", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=CLG%3A&room=CLG%3AK402");
		buildingMap.get("Carl L. Godskes hus").put("Kollokvierom 403 (\"beta\")", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=CLG%3A&room=CLG%3AK403");
		buildingMap.get("Carl L. Godskes hus").put("Kollokvierom 404 (\"gamma\")", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=CLG%3A&room=CLG%3AK404");
		buildingMap.get("Carl L. Godskes hus").put("M�te-/koll.rom 508", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=CLG%3A&room=CLG%3AK508");
		buildingMap.get("Carl L. Godskes hus").put("M�te-/koll.rom 510", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=CLG%3A&room=CLG%3AK510");
		buildingMap.get("Carl L. Godskes hus").put("Seminarrom 528", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=CLG%3A&room=CLG%3AK528");
		
		buildingMap.put("Dragefjellet", new HashMap<String, String>());
		buildingMap.get("Dragefjellet").put("Auditorium 1 (604)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3AA1");
		buildingMap.get("Dragefjellet").put("Auditorium 2 (603)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3AA2");
		buildingMap.get("Dragefjellet").put("Auditorium 3 (602)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3AA3");
		buildingMap.get("Dragefjellet").put("Auditorium 4 (034)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3AA4");
		buildingMap.get("Dragefjellet").put("Kollokvierom 1 (405)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3AK1");
		buildingMap.get("Dragefjellet").put("Kollokvierom 2 (406)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3AK2");
		buildingMap.get("Dragefjellet").put("Kollokvierom 3 (407)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3AK3");
		buildingMap.get("Dragefjellet").put("Kollokvierom 4 (409)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3AK4");
		buildingMap.get("Dragefjellet").put("Kollokvierom 5 (410)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3AK5");
		buildingMap.get("Dragefjellet").put("Seminarrom 1 (404)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3AS1");
		buildingMap.get("Dragefjellet").put("Seminarrom 2 (408)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3AS2");
		buildingMap.get("Dragefjellet").put("Seminarrom 3 (412)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3AS3");
		buildingMap.get("Dragefjellet").put("Seminarrom 4 (413)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3AS4");
		buildingMap.get("Dragefjellet").put("Seminarrom 5 (414)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3AS5");
		buildingMap.get("Dragefjellet").put("Seminarrom A (403)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3ASA");
		buildingMap.get("Dragefjellet").put("Seminarrom B (404A)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3ASB");
		buildingMap.get("Dragefjellet").put("Seminarrom C (404B)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3ASC");
		buildingMap.get("Dragefjellet").put("Seminarrom D (306)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3ASD");
		buildingMap.get("Dragefjellet").put("Seminarrom E (307)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3ASE");
		buildingMap.get("Dragefjellet").put("Seminarrom F (210)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=DR%3A&room=DR%3ASF");
		
		buildingMap.put("Gamle hovedbygning - Haukeland Sykehus", new HashMap<String, String>());
		buildingMap.get("Gamle hovedbygning - Haukeland Sykehus").put("Olavssalen (2101)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=GH%3A&room=GH%3AOLAVS");
		
		buildingMap.put("Griegakademiet", new HashMap<String, String>());
		buildingMap.get("Griegakademiet").put("M�terom 110", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=GR%3A&room=GR%3A110");
		buildingMap.get("Griegakademiet").put("Undervisningsrom 206", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=GR%3A&room=GR%3A206");
		buildingMap.get("Griegakademiet").put("Undervisningsrom 209", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=GR%3A&room=GR%3A209");
		buildingMap.get("Griegakademiet").put("Undervisningsrom 210", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=GR%3A&room=GR%3A210");
		buildingMap.get("Griegakademiet").put("Undervisningsrom 421", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=GR%3A&room=GR%3A421");
		buildingMap.get("Griegakademiet").put("Gunnar S�vigs Sal (138)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=GR%3A&room=GR%3AGSS");
		buildingMap.get("Griegakademiet").put("Pr�vesalen (331S)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=GR%3A&room=GR%3APR%D8VE");
		
		buildingMap.put("Haraldsplass Diakonale Sykehus", new HashMap<String, String>());
		buildingMap.get("Haraldsplass Diakonale Sykehus").put("Damsg�rd (appendix)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HDS%3A&room=HDS%3ADAMSG%C5RD");
		buildingMap.get("Haraldsplass Diakonale Sykehus").put("Fl�yen (appendix)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HDS%3A&room=HDS%3AFL%D8YEN");
		buildingMap.get("Haraldsplass Diakonale Sykehus").put("Isdalen (appendix)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HDS%3A&room=HDS%3AISDALEN");
		buildingMap.get("Haraldsplass Diakonale Sykehus").put("Lyderhorn (appendix)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HDS%3A&room=HDS%3ALYDERHORN");
		
		buildingMap.put("Herman Foss gate 6", new HashMap<String, String>());
		buildingMap.get("Herman Foss gate 6").put("Seminarrom 327", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HF6%3A&room=HF6%3A327");
		buildingMap.get("Herman Foss gate 6").put("M�terom 408", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HF6%3A&room=HF6%3A408");
		
		buildingMap.put("HF-bygget", new HashMap<String, String>());
		buildingMap.get("HF-bygget").put("Datalab 123", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HF%3A&room=HF%3A123");
		buildingMap.get("HF-bygget").put("Lydlab 127", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HF%3A&room=HF%3A127");
		buildingMap.get("HF-bygget").put("Seminarrom 216", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HF%3A&room=HF%3A216");
		buildingMap.get("HF-bygget").put("Seminarrom 217", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HF%3A&room=HF%3A217");
		buildingMap.get("HF-bygget").put("IKT-seminarrom 264", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HF%3A&room=HF%3A264");
		buildingMap.get("HF-bygget").put("IKT-seminarrom 265", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HF%3A&room=HF%3A265");
		buildingMap.get("HF-bygget").put("Seminarrom 301", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HF%3A&room=HF%3A301");
		buildingMap.get("HF-bygget").put("Seminarrom 326", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HF%3A&room=HF%3A326");
		buildingMap.get("HF-bygget").put("Seminarrom 353", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HF%3A&room=HF%3A353");
		buildingMap.get("HF-bygget").put("Seminarrom 371", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HF%3A&room=HF%3A371");
		buildingMap.get("HF-bygget").put("Seminarrom 400", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HF%3A&room=HF%3A400");
		
		buildingMap.put("H�yteknologisenteret", new HashMap<String, String>());
		buildingMap.get("H�yteknologisenteret").put("Laboratorium", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HIB%3A&room=HIB%3A325C1");
		buildingMap.get("H�yteknologisenteret").put("Kurssal 437A1", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HIB%3A&room=HIB%3A437A1");
		buildingMap.get("H�yteknologisenteret").put("Seminarrom 520B", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HIB%3A&room=HIB%3A520B1");
		buildingMap.get("H�yteknologisenteret").put("Datalab 1130 (1. etg.)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HIB%3A&room=HIB%3AFLAB1");
		buildingMap.get("H�yteknologisenteret").put("Datalab 1129 (1. etg.)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HIB%3A&room=HIB%3AFLAB2");
		buildingMap.get("H�yteknologisenteret").put("Datalab 1128 (1. etg.)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HIB%3A&room=HIB%3AFLAB3");
		buildingMap.get("H�yteknologisenteret").put("Datalab 1107 (1. etg.)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HIB%3A&room=HIB%3AFLAB4");
		buildingMap.get("H�yteknologisenteret").put("Grupperom 2101", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HIB%3A&room=HIB%3AG2101");
		buildingMap.get("H�yteknologisenteret").put("Grupperom 2103", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HIB%3A&room=HIB%3AG2103");
		buildingMap.get("H�yteknologisenteret").put("Grupperom 2104", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HIB%3A&room=HIB%3AG2104");
		buildingMap.get("H�yteknologisenteret").put("Grupperom 2143", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HIB%3A&room=HIB%3AG2143");
		buildingMap.get("H�yteknologisenteret").put("Lille auditorium (2142)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HIB%3A&room=HIB%3AG2143");
		buildingMap.get("H�yteknologisenteret").put("Stort auditorium (2144)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HIB%3A&room=HIB%3ASA");
		
		buildingMap.put("Haukeland Universitetssykehus", new HashMap<String, String>());
		buildingMap.get("Haukeland Universitetssykehus").put("Auditorium B301", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AB301");
		buildingMap.get("Haukeland Universitetssykehus").put("Auditorium B302", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AB302");
		buildingMap.get("Haukeland Universitetssykehus").put("Undervisningsrom B306 (4504)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AB306");
		buildingMap.get("Haukeland Universitetssykehus").put("Undervisningsrom B307 (4505)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AB307");
		buildingMap.get("Haukeland Universitetssykehus").put("Undervisningsrom D301 (4517)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AD301");
		buildingMap.get("Haukeland Universitetssykehus").put("Undervisningsrom D302 (4518)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AD302");
		buildingMap.get("Haukeland Universitetssykehus").put("Birkhaugsalen (4519)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AD303");
		buildingMap.get("Haukeland Universitetssykehus").put("Undervisningsrom D304", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AD304");
		buildingMap.get("Haukeland Universitetssykehus").put("Kursrom D305", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AD305");
		buildingMap.get("Haukeland Universitetssykehus").put("Kursrom E231", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AE231");
		buildingMap.get("Haukeland Universitetssykehus").put("Auditorium H113", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AH113");
		buildingMap.get("Haukeland Universitetssykehus").put("Auditorium HUD", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AHUD");
		buildingMap.get("Haukeland Universitetssykehus").put("Seminarrom M101", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AM101");
		buildingMap.get("Haukeland Universitetssykehus").put("Bibliotek M102", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AM102");
		buildingMap.get("Haukeland Universitetssykehus").put("M. Haalands kurssal", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AN201");
		buildingMap.get("Haukeland Universitetssykehus").put("Stort auditorium (4601)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3ASA");
		buildingMap.get("Haukeland Universitetssykehus").put("Kursrom T218", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3AT218");
		buildingMap.get("Haukeland Universitetssykehus").put("Auditorium �NH", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=HS%3A&room=HS%3A%D8NH");
		
		buildingMap.put("Jahnebakken 3", new HashMap<String, String>());
		buildingMap.get("Jahnebakken 3").put("Auditorium 105", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=JB3%3A&room=JB3%3AA");
		
		buildingMap.put("John Lunds plass 3", new HashMap<String, String>());
		buildingMap.get("John Lunds plass 3").put("Seminarrom 106", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=JLP3%3A&room=JLP3%3A106");
		buildingMap.get("John Lunds plass 3").put("Grupperom 210 (felles l�rerstudio)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=JLP3%3A&room=JLP3%3A210");
		
		buildingMap.put("Jekteviksbakken 31 (Jus II)", new HashMap<String, String>());
		buildingMap.get("Jekteviksbakken 31 (Jus II)").put("Auditorium (145)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=JVB31%3A&room=JVB31%3AAUD");
		buildingMap.get("Jekteviksbakken 31 (Jus II)").put("Seminarrom 1 (135)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=JVB31%3A&room=JVB31%3AS1");
		buildingMap.get("Jekteviksbakken 31 (Jus II)").put("Seminarrom 2 (133)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=JVB31%3A&room=JVB31%3AS2");
		buildingMap.get("Jekteviksbakken 31 (Jus II)").put("Seminarrom 3 (132)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=JVB31%3A&room=JVB31%3AS3");
		buildingMap.get("Jekteviksbakken 31 (Jus II)").put("Seminarrom 4 (131)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=JVB31%3A&room=JVB31%3AS4");
		buildingMap.get("Jekteviksbakken 31 (Jus II)").put("Seminarrom 6 (143)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=JVB31%3A&room=JVB31%3AS6");
		buildingMap.get("Jekteviksbakken 31 (Jus II)").put("Seminarrom 7 (144)", "http://rom.app.uib.no/ukesoversikt/?entry=byggrom&building=JVB31%3A&room=JVB31%3AS7");
		
	}
}
